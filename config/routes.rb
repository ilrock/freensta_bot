Rails.application.routes.draw do
  root to: 'pages#home'

  namespace :api do 
  	namespace :v1 do 
  		resources :hashtags, only: [:index, :create, :destroy, :update]
      resources :comments, only: [:index, :create, :destroy, :update]
  		resources :followers, only: [:index, :create, :destroy, :update]
  		resources :followings, only: [:index, :create, :destroy, :update]
  		get 'access/instagram', to: 'access#instagram'
      post 'bot/start', to: "bot#start"
  	end 
  end

  get '/oauth/connect', to: 'oauth#connect'
  get '/oauth/callback', to: 'oauth#callback'
end
