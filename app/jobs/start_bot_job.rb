class StartBotJob < ApplicationJob
  	#include Sidekiq::Worker
  	include ActiveJobStatus::Hooks
  	
  	def true?(obj)
	  obj.to_s == "true"
	end

  	def perform(follow_limit, like_limit, comment_limit, insta_username, insta_password, allow_unfollows)
	  	#Auth data
	    username = insta_username
		password = insta_password
		
		# List of target hashtags
		hashtags = Hashtag.all.map{|hashtag| hashtag.name}
		
		# Follow and unfollow buttons
		follow_class = "_qv64e _gexxb _4tgw8 _njrw0"
		unfollow_class = "_qv64e _t78yp _4tgw8 _njrw0"
		
		# Number of followed accounts, unfollowed accounts and liked pics
		follow_counter = 0
		like_counter = 0
		comment_counter = 0
		
		# Limits to follows, likes and comments
		max_follows = follow_limit.to_i || 350
		max_likes = like_limit.to_i || 525
		max_comments = comment_limit.to_i || 250
		allow_unfollows = true?(allow_unfollows)

		follows_per_hashtag = max_follows / hashtags.length
		likes_per_hashtag = max_likes / hashtags.length
		comments_per_hashtag = max_comments / hashtags.length

		# Start time
		start_time = Time.now

		# Open Browser, Navigate to Login page
		browser = Watir::Browser.new :chrome, switches: ['--incognito']
		browser.goto "https://instagram.com/accounts/login/"

		# Navigate to Username and Password fields, inject info
		puts "Logging in..."
		browser.text_field(:name => "username").set "#{username}"
		browser.text_field(:name => "password").set "#{password}"

		# Click Login Button
		browser.button(:class => '_qv64e _gexxb _4tgw8 _njrw0').click
		sleep(4)
		puts "We're in #hackerman"

		start_from = start_from || 0
		finish_at = finish_at || [follows_per_hashtag, likes_per_hashtag, comments_per_hashtag].max
		loop do
			hashtags.each do |hashtag|
				# Navigate to hashtag's page
			    browser.goto "https://instagram.com/explore/tags/#{hashtag}"

			    # Scroll as needed to show enough images
				if [follows_per_hashtag, likes_per_hashtag, comments_per_hashtag].max > 21
					iterations = ([follows_per_hashtag, likes_per_hashtag, comments_per_hashtag].max.to_f/21).ceil - 1
					if browser.a(:class => "_1cr2e _epyes").exists?
						browser.a(:class => "_1cr2e _epyes").click
						sleep(1)
					end
					iterations.times do
					    browser.driver.execute_script("window.scrollBy(0,document.body.scrollHeight)")
					    sleep(1)
					end
				end
		
			    # If there are pictures in the hashtag
			    if browser.div(:class => '_mck9w _gvoze _f2mse').exists?
		    		#start the counter
		    		counter = 0
		    		browser.divs(:class => '_mck9w _gvoze _f2mse')[start_from..finish_at - 1].each_with_index do |picture|
		    			# Open the picture
		    			picture.click
		    			sleep(2)
		    			# Follow or unfollow JUST if we haven't reached the FOLLOWS_PER_HASHTAG limit
		    			if counter < follows_per_hashtag && follow_counter < max_follows
		    			 	follow_action(browser, follow_class, unfollow_class, allow_unfollows, follow_counter)
		    				follow_counter += 1
		    			end
		    			sleep(2)
		    			# Like JUST if we haven't reached the LIKES_PER_HASHTAG limit
		    			if counter < likes_per_hashtag && like_counter < max_likes
		    				like_action(browser, "_8scx2 coreSpriteHeartOpen", like_counter)
		    			end
		    			sleep(2)
		    			# Comment JUST if we haven't reached the COMMENT_PER_HASHTAG limit
		    			if counter < comments_per_hashtag && comment_counter < max_comments
		    				comment_action(browser, username, comment_counter)
		    				comment_counter += 1
		    			end
		    			sleep(2)
		    			# Increase the counter
		    			counter+= 1

		    			ap "====================== Followed so far: #{follow_counter}"
		    			ap "====================== Liked so far: #{like_counter}"
		    			ap "====================== Commented so far: #{comment_counter}"

		    			# Close the picture
		    			if browser.button(:class => '_dcj9f').exists?
		    				browser.button(:class => '_dcj9f').click
		    			end

		    			sleep(30)
		    			
		    		end
		    	end
		    end
		    start_from = finish_at + 1
			finish_at = start_from + [follows_per_hashtag, likes_per_hashtag, comments_per_hashtag].max
			ap "====================== START FROM ==========================="
			ap "====================== #{start_from} ========================"
			ap "====================== FINISH AT ==========================="
			ap "====================== #{finish_at} ========================"
			ap "====================== MAX FOLLOWS | FOLLOW COUNTER ==========================="
			ap "====================== #{max_follows} | #{follow_counter} ========================"
			ap "====================== MAX LIKES | LIKE COUNTER ==========================="
			ap "====================== #{max_likes} | #{like_counter} ========================" 
			ap "====================== MAX COMMENTS | COMMENT COUNTER ==========================="
			ap "====================== #{max_comments} | #{comment_counter} ========================"
			
			if max_follows == follow_counter && max_likes == like_counter && max_comments == comment_counter
				follow_counter = 0
				like_counter = 0
				comment_counter = 0
				sleep(1.day)
			end
		end
		# Leave this in to use the REPL at end of program
		# Otherwise, take it out and program will just end
		Pry.start(binding)
  	end

  	def follow_action(browser, follow_class, unfollow_class, allow_unfollows, follow_counter)
	  	if browser.button(:class => follow_class).exists?
	      	ap "Following account"
	      	browser.button(:class => follow_class).click
	      	follow_counter += 1
	    elsif browser.button(:class => unfollow_class).exists? && allow_unfollows
	      	ap "Unfollowing account"
	      	browser.button(:class => unfollow_class).click
	      	follow_counter += 1
	    end
  	end

 	def like_action(browser, class_name, like_counter)
  		if browser.span(:class => class_name).exists?
      		ap "Liking picture"
      		like_counter += 1
      		browser.span(:class => class_name).click
    	end
  	end

  	def comment_action(browser, username, comment_counter)
  		if browser.form(:class => "_b6i0l").exists? && !have_commented(browser, username)
  			if browser.form(:class => "_b6i0l").textarea.exists?
  				ap "Commenting post"
  				comment_counter =+ 1
  				browser.form(:class => "_b6i0l").textarea.set(Comment.all.sample.body)
  				browser.send_keys :enter
  			end
  		end
  	end

  	def have_commented(browser, username)
  		if browser.a(:class => "_2g7d5 notranslate _95hvo").exists?
  			browser.as(:class => "_2g7d5 notranslate _95hvo").each do |a|
  				return true if a.text == username
  			end
  			return false
  		end
  	end
end
class ApplicationJob < ActiveJob::Base
end