class HashtagsController < ApplicationController
  def create
    @hashtag = Hashtag.new(hashtag_params)
    if @hashtag.save
      render json: @hashtag
    else
      render json: @hashtag.errors, status: :unprocessable_entity
    end
  end

  private
    
    def hashtag_params
      params.require(:hashtag).permit(:name)
    end
end
