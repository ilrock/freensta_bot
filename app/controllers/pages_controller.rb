class PagesController < ApplicationController
  def home
    @followers = Follower.all
    @followings = Following.all
    @hashtags = Hashtag.all
  end
end
