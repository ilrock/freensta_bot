class Api::V1::FollowingsController < Api::V1::BaseController 
	def index 
		respond_with Following.all 
	end 

	def create 
		respond_with :api, :v1, Following.create(following_params) 
	end 

	def destroy 
		respond_with Following.destroy(params[:id]) 
	end 

	def update 
		following = Following.find(params["id"]) 
		following.update_attributes(following_params) 
		respond_with following, json: following 
	end 

	private 

	def following_params 
		params.require(:following).permit(:id, :name, :description) 
	end 
end
