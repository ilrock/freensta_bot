class Api::V1::HashtagsController < Api::V1::BaseController	
	def index 
		respond_with Hashtag.all 
	end 

	def create
		hashtags = params[:hashtag][:name].gsub(/\s+/, "").split(",").map {|hashtag| Hashtag.create(name: hashtag) }
		respond_with :api, :v1, json: hashtags
	end

	def destroy 
		respond_with Hashtag.destroy(params[:id]) 
	end 

	def update 
		hashtag = Hashtag.find(params["id"]) 
		hashtag.update_attributes(hashtag_params) 
		respond_with hashtag, json: hashtag 
	end 

	private 

	def hashtag_params 
		params.require(:hashtag).permit(:id, :name) 
	end 
end
