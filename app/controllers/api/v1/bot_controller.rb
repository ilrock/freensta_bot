class Api::V1::BotController < Api::V1::BaseController	
	def start
		bot_job = StartBotJob.perform_now(params[:follow_limit], params[:like_limit], params[:comment_limit], params[:username], params[:password], params[:allow_unfollows])
		respond_with :api, :v1, ActiveJobStatus.fetch(bot_job.job_id)
	end
end
