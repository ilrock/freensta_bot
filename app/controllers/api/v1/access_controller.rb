class Api::V1::AccessController < Api::V1::BaseController 
  def instagram
    respond_with session[:access_token] && session[:access_token] != ""
  end
end
