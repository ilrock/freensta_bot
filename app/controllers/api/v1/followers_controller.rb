class Api::V1::FollowersController < Api::V1::BaseController 
	def index 
		respond_with Follower.all 
	end 

	def create 
		respond_with :api, :v1, Follower.create(follower_params) 
	end 

	def destroy 
		respond_with Follower.destroy(params[:id]) 
	end 

	def update 
		follower = Follower.find(params["id"]) 
		follower.update_attributes(follower_params) 
		respond_with follower, json: follower 
	end

	def pull
	
	end 

	private 

	def follower_params 
		params.require(:follower).permit(:id, :name, :description) 
	end 
end
