class OauthController < ApplicationController
  def connect
    redirect_to Instagram.authorize_url(:redirect_uri => "http://localhost:4003/oauth/callback", :scope => "follower_list")
  end

  def callback
  	response = Instagram.get_access_token(params[:code], :redirect_uri => "http://localhost:4003/oauth/callback")
  	cookies[:access_token] = response.access_token
    session[:access_token] = response.access_token
    redirect_to root_url
  end
end
