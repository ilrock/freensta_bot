var Body = React.createClass({ 
	getInitialState() { 
		return { 
			hashtags: [],
			followers: [],
			followings: [],
			comments: [],
			authenticated: false
		} 
	},

	componentDidMount() { 
		// Get all the hashtags
		$.getJSON('/api/v1/hashtags.json', (response) => { this.setState({ hashtags: response }) });

		//Get all the followers
		$.getJSON('/api/v1/followers.json', (response) => { this.setState({ followers: response }) });

		//Get all the followings
		$.getJSON('/api/v1/followings.json', (response) => { this.setState({ followings: response }) });

		//Get all the comments
		$.getJSON('/api/v1/comments.json', (response) => { this.setState({ comments: response }) });

		$.getJSON('/api/v1/access/instagram.json', (response) => { 
			newState = response
			console.log(newState);
			this.setState({
				authenticated: newState
			});
		});
	},

	createHashtag(hashtags) {
        console.log(hashtags);
        var newState = this.state.hashtags.concat(hashtags); 
        this.setState({ hashtags: newState })
    },

    deleteHashtag(id) { 
    	$.ajax({ 
    		url: `/api/v1/hashtags/${id}`, 
    		type: 'DELETE', 
    		success:() => { 
    			this.removeHashtagClient(id); 
    		} 
    	}); 
    }, 

    removeHashtagClient(id) { 
    	var newHashtags = this.state.hashtags.filter((hashtag) => { 
    		return hashtag.id != id; 
    	}); 
    	this.setState({ 
    		hashtags: newHashtags 
    	}); 
    },

    createComment(comments) {
        console.log(comments);
        var newState = this.state.comments.concat(comments); 
        this.setState({ comments: newState })
    },

    deleteComment(id) { 
    	$.ajax({ 
    		url: `/api/v1/comments/${id}`, 
    		type: 'DELETE', 
    		success:() => { 
    			this.removeCommentClient(id); 
    		} 
    	}); 
    }, 

    removeCommentClient(id) { 
    	var newComments = this.state.comments.filter((comment) => { 
    		return comment.id != id; 
    	}); 
    	this.setState({ 
    		comments: newComments 
    	}); 
    },

    isUserAuthenticated(){
    	var newState;
    	console.log('newState');
    	$.getJSON('/api/v1/access/instagram.json', (response) => { newState = response })
    	this.setState({
    		authenticated: newState
    	});
    },

    startBot() { 
    	var follow_limit = this.refs.follow_limit.value;
    	var like_limit = this.refs.like_limit.value;
    	var comment_limit = this.refs.comment_limit.value;
    	var username = this.refs.username.value;
    	var password = this.refs.password.value;
    	var allow_unfollows = this.refs.allow_unfollows.checked;
		$.ajax({ 
			url: '/api/v1/bot/start', 
			type: 'POST',
			data: { 
				follow_limit: follow_limit,
				like_limit: like_limit,
				comment_limit: comment_limit,
				username: username,
				password: password,
				allow_unfollows: allow_unfollows
			}, 
			success: (hashtags) => { 
				this.props.handleSubmit(hashtags);
			} 
		});
	},

	render() {
		var instagram_html;
		if (this.state.authenticated){
			instagram_html = <a href="#"> Get your data </a>
		} else{
			instagram_html = <a href="/oauth/connect">Connect with Instagram</a>
		}

		return ( 
			<div className="row">
				<div className="col-md-6">
					<div className="row">
						<div className="col-12">
							<div className="bot-section">
								<NewHashtag createHashtag={this.createHashtag} />
							</div>
						</div>
						<div className="col-12">
							<div className="bot-section">
								<AllHashtags hashtags={this.state.hashtags} deleteHashtag={this.deleteHashtag}/>
							</div>
						</div>
					</div>
				</div>
				<div className="col-md-6">
					<div className="row">
						<div className="col-12">
							<div className="bot-section">
								<NewComment createComment={this.createComment} />
							</div>
						</div>
						<div className="col-12">
							<div className="bot-section">
								<AllComments comments={this.state.comments} deleteComment={this.deleteComment}/>
							</div>
						</div>
					</div>
				</div>
				<div className="col-md-6">
					<div className="bot-section">
						<AllFollowers followers={this.state.followers} />
					</div>
				</div>
				<div className="col-md-6">
					<div className="bot-section">
						<AllFollowings followings={this.state.followings}/>
					</div>
				</div>
				<div className="col-12">
					{/*instagram_html*/}
					<div className="row bot-section">
						<div className="col-12">
							<h2> Start Your Bot </h2>
						</div>
						<div className="col-md-5 form-group">
							<input ref='username' placeholder='Instagram username' className="form-control"/> 
						</div>
						<div className="col-md-5 form-group">
							<input ref='password' type="password" placeholder='Instagram password' className="form-control"/> 
						</div>
						<div className="col-md-2 form-group">
							<label> Allow Unfollows: <input ref="allow_unfollows" type="checkbox"/></label>
						</div>
						<div className="col-md-4 form-group">
							<input ref='follow_limit' placeholder='Follow limit: default 350' className="form-control"/> 
						</div>
						<div className="col-md-4 form-group">
							<input ref='like_limit' placeholder='Like limit: default: 525' className="form-control"/> 
						</div>
						<div className="col-md-4 form-group">
							<input ref='comment_limit' placeholder='Comment limit: default 250' className="form-control"/> 
						</div>
						<div className="col-md-12 form-group">
							<button className="btn btn-success" onClick={this.startBot} style={{width: "100%"}}> Start </button>
						</div>
					</div>
				</div>
			</div>
		)
	} 
});
