var NewHashtag= React.createClass({
	createHashtag() { 
		var name = this.refs.name.value; 
		console.log('The name value is ' + name);
		$.ajax({ 
			url: '/api/v1/hashtags', 
			type: 'POST', 
			data: { 
				hashtag: { 
					name: name 
				}
			}, 
			success: (hashtags) => { 
				this.props.createHashtag(hashtags);
			} 
		});
	},
	render() { 
		return ( 
			<div className="row form-group" style={{padding: "15px 15px 0 15px"}}>
				<div className="col-md-9">
					<input ref='name' placeholder='Enter the target hashtags separated by comma' className="form-control"/> 
				</div>
				<div className="col-md-3">
					<button className="btn btn-success" onClick={this.createHashtag} style={{width: "100%"}}>Submit</button>
				</div>
			</div> 
		) 
	} 
});
