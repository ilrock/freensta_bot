var AllFollowings = React.createClass({ 
	render() {
		var followings= this.props.followings.map((following) => { 
			return ( 
				<div key={following.id}> 
					<span>{following.name}</span> 
				</div> 
			) 
		}); 

		return( 
			<div> 
				<h2> You're Following </h2>
				{followings} 
			</div> 
		) 
	}
});
