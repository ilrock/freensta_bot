var AllFollowers = React.createClass({ 
	render() {
		var followers= this.props.followers.map((follower) => { 
			return ( 
				<div key={follower.id}> 
					<span>{follower.name}</span> 
				</div> 
			) 
		}); 

		return( 
			<div>
				<h2> Your Followers </h2>
				{followers} 
			</div> 
		) 
	}
});
