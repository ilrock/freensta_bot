var NewComment= React.createClass({
	createComment() { 
		var body = this.refs.body.value; 
		console.log('The name value is ' + body);
		$.ajax({ 
			url: '/api/v1/comments', 
			type: 'POST', 
			data: { 
				comment: { 
					body: body 
				}
			}, 
			success: (comment) => { 
				this.props.createComment(comment);
			} 
		});
	},
	render() { 
		return ( 
			<div className="row form-group" style={{padding: "15px 15px 0 15px"}}>
				<div className="col-md-9">
					<input ref='body' type="text" placeholder='Enter the comments you want to use' className="form-control"/> 
				</div>
				<div className="col-md-3">
					<button className="btn btn-success" onClick={this.createComment} style={{width: "100%"}}>Submit</button>
				</div>
			</div> 
		) 
	} 
});
