var AllHashtags = React.createClass({ 
    deleteHashtag(id) {
	    this.props.deleteHashtag(id);
	},

	render() {
		var hashtags= this.props.hashtags.map((hashtag) => { 
			return (
				<tr key={hashtag.id}>
			      <td> {hashtag.name} </td>
			      <td> {Math.floor(Math.random()*10000)} </td>
			      <td> <button className="btn btn-danger" onClick={this.deleteHashtag.bind(this, hashtag.id)}>Delete</button> </td>
			    </tr> 
			) 
		}); 

		return(
			<table className="table">
			  <thead>
			    <tr>
			      <th>Hashtag</th>
			      <th>Popularity</th>
			      <th>Delete</th>
			    </tr>
			  </thead>
			  <tbody>
			    {hashtags}
			  </tbody>
			</table>
		) 
	}
});
