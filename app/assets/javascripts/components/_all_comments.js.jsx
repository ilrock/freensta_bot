var AllComments = React.createClass({ 
    deleteComment(id) {
	    this.props.deleteComment(id);
	},

	render() {
		var comments= this.props.comments.map((comment) => { 
			return (
				<tr key={comment.id}>
			      <td> {comment.body} </td>
			      <td> <button className="btn btn-danger" onClick={this.deleteComment.bind(this, comment.id)}>Delete</button> </td>
			    </tr> 
			) 
		}); 

		return(
			<table className="table">
			  <thead>
			    <tr>
			      <th>Comment</th>
			      <th>Delete</th>
			    </tr>
			  </thead>
			  <tbody>
			    {comments}
			  </tbody>
			</table>
		) 
	}
});
